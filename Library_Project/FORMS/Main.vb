﻿Imports MySql.Data.MySqlClient

Public Class Main
    Public StringResult As String
    Public mySqlCommand As New MySqlCommand
    Public mySqlAdapter As New MySqlDataAdapter


    'Public Function StartMySqlConnection() As MySqlConnection
    '    Return New MySqlConnection("server=localhost;user id=root;password=;database=online_tech_db")
    'End Function


    Function StartMySqlConnection() As MySqlConnection
        Return New MySqlConnection("server='" & My.Settings.Server & "';user id='" & My.Settings.Username & "';password='" & My.Settings.Password & "';database='" & My.Settings.Database & "'")
    End Function



    Public Function PopulateTable(UpdatedmySqlDataTable As DataTable, mySqlCon As MySqlConnection)
        Try
            GRID_Library.DataSource = Nothing
            GRID_Library.Rows.Clear()
            GRID_Library.Columns.Clear()

            mySqlCon.Open()
            With mySqlCommand
                .Connection = mySqlCon
                .CommandText = "SELECT db_col_fullname,db_col_age,db_col_address,db_col_uid FROM online_tech_tbl"
            End With
            mySqlAdapter.SelectCommand = mySqlCommand
            mySqlAdapter.Fill(UpdatedmySqlDataTable)
            With GRID_Library
                .DataSource = UpdatedmySqlDataTable
                .AllowUserToAddRows = False
                .AllowUserToDeleteRows = False
                .Columns(0).HeaderText = "FullName"
                .Columns(1).HeaderText = "Age"
                .Columns(2).HeaderText = "Number"
                .Columns(3).HeaderText = "UID"
                .Columns(0).Width = 110
                .Columns(1).Width = 30
                .Columns(2).Width = 110
                .Columns(3).Width = 20
                .Columns(3).Visible = False
            End With
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical)
        End Try
        mySqlCon.Close()


    End Function


    Private Sub BTN_OPTION_Click(sender As Object, e As EventArgs) Handles BTN_OPTION.Click
        Me.Hide()
        Options.Show()
    End Sub

    Private Sub Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim UpdatedmySqlDataTable As New DataTable
        Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
        PopulateTable(UpdatedmySqlDataTable, MySqlConnection)
    End Sub

    Private Sub BTN_Save_Click(sender As Object, e As EventArgs) Handles BTN_Save.Click

        If ((TXT_fullname.Text IsNot "") And (TXT_Age.Text IsNot "") And (TXT_Address.Text IsNot "")) Then
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            Try
                MySqlConnection.Open()
                With mySqlCommand
                    .Connection = MySqlConnection
                    .CommandText = "INSERT INTO online_tech_tbl(`db_col_fullname`,`db_col_age`,`db_col_address`) VALUES ('" & TXT_fullname.Text & "','" & TXT_Age.Text & "','" & TXT_Address.Text & "')"
                    Dim mysql_result = mySqlCommand.ExecuteNonQuery

                    If mysql_result = 0 Then
                        MsgBox("Data not save to MySQL Database.", vbCritical)
                    Else
                        MsgBox("Data has been saved to MySQL Database", vbInformation)
                        TXT_fullname.Text = ""
                        TXT_Age.Text = ""
                        TXT_Address.Text = ""
                    End If
                End With
            Catch ex As Exception
                MsgBox(ex.Message, vbCritical)
            End Try
            MySqlConnection.Close()

        Else
            MsgBox("All Forms Required!", vbCritical)
        End If


    End Sub

    Private Sub BTN_Update_Click(sender As Object, e As EventArgs) Handles BTN_Update.Click
        Me.Hide()
        Edit.Show()

    End Sub

    Private Sub BTN_Delete_Click(sender As Object, e As EventArgs) Handles BTN_Delete.Click


        Dim ConfirmDialog As DialogResult = MessageBox.Show("Are sure to delete the Selected Item?", "Action Confirm", MessageBoxButtons.YesNo)

        If (ConfirmDialog = 6) Then
            Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
            Try
                MySqlConnection.Open()
                With mySqlCommand
                    .Connection = MySqlConnection
                    .CommandText = "DELETE FROM online_tech_tbl WHERE db_col_uid='" & GRID_Library.Rows(GRID_Library.CurrentRow.Index).Cells(3).Value.ToString & "'"
                    Dim mysql_result = mySqlCommand.ExecuteNonQuery

                    If mysql_result = 0 Then
                        MsgBox("Data cannot remove to MySQL Database.", vbCritical)
                    Else
                        MsgBox("Data has been removed to MySQL Database", vbInformation)
                        mySqlConnection.Close()
                        Dim UpdatedmySqlDataTable As New DataTable
                        PopulateTable(UpdatedmySqlDataTable, MySqlConnection)
                    End If
                End With
            Catch ex As Exception
                MsgBox(ex.Message, vbCritical)
            End Try

        Else
            MsgBox("Cancelled")
            GRID_Library.Refresh()
        End If





    End Sub

End Class
