﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Edit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Edit))
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TXT_Address = New System.Windows.Forms.TextBox()
        Me.TXT_Age = New System.Windows.Forms.TextBox()
        Me.TXT_fullname = New System.Windows.Forms.TextBox()
        Me.BTN_Update = New System.Windows.Forms.Button()
        Me.TXT_UID = New System.Windows.Forms.TextBox()
        Me.BTN_Cancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(27, 166)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 20)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Address:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(27, 99)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 20)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Age:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(27, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 20)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Fullname:"
        '
        'TXT_Address
        '
        Me.TXT_Address.Location = New System.Drawing.Point(27, 189)
        Me.TXT_Address.Multiline = True
        Me.TXT_Address.Name = "TXT_Address"
        Me.TXT_Address.Size = New System.Drawing.Size(237, 36)
        Me.TXT_Address.TabIndex = 16
        '
        'TXT_Age
        '
        Me.TXT_Age.Location = New System.Drawing.Point(27, 122)
        Me.TXT_Age.Multiline = True
        Me.TXT_Age.Name = "TXT_Age"
        Me.TXT_Age.Size = New System.Drawing.Size(237, 36)
        Me.TXT_Age.TabIndex = 15
        '
        'TXT_fullname
        '
        Me.TXT_fullname.Location = New System.Drawing.Point(27, 60)
        Me.TXT_fullname.Multiline = True
        Me.TXT_fullname.Name = "TXT_fullname"
        Me.TXT_fullname.Size = New System.Drawing.Size(237, 36)
        Me.TXT_fullname.TabIndex = 14
        '
        'BTN_Update
        '
        Me.BTN_Update.Location = New System.Drawing.Point(27, 256)
        Me.BTN_Update.Name = "BTN_Update"
        Me.BTN_Update.Size = New System.Drawing.Size(132, 43)
        Me.BTN_Update.TabIndex = 13
        Me.BTN_Update.Text = "Update"
        Me.BTN_Update.UseVisualStyleBackColor = True
        '
        'TXT_UID
        '
        Me.TXT_UID.Location = New System.Drawing.Point(27, 12)
        Me.TXT_UID.Multiline = True
        Me.TXT_UID.Name = "TXT_UID"
        Me.TXT_UID.Size = New System.Drawing.Size(69, 23)
        Me.TXT_UID.TabIndex = 20
        '
        'BTN_Cancel
        '
        Me.BTN_Cancel.Location = New System.Drawing.Point(170, 256)
        Me.BTN_Cancel.Name = "BTN_Cancel"
        Me.BTN_Cancel.Size = New System.Drawing.Size(132, 43)
        Me.BTN_Cancel.TabIndex = 21
        Me.BTN_Cancel.Text = "Cancel"
        Me.BTN_Cancel.UseVisualStyleBackColor = True
        '
        'Edit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(419, 425)
        Me.Controls.Add(Me.BTN_Cancel)
        Me.Controls.Add(Me.TXT_UID)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TXT_Address)
        Me.Controls.Add(Me.TXT_Age)
        Me.Controls.Add(Me.TXT_fullname)
        Me.Controls.Add(Me.BTN_Update)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Edit"
        Me.Text = "Update"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TXT_Address As TextBox
    Friend WithEvents TXT_Age As TextBox
    Friend WithEvents TXT_fullname As TextBox
    Friend WithEvents BTN_Update As Button
    Friend WithEvents TXT_UID As TextBox
    Friend WithEvents BTN_Cancel As Button
End Class
