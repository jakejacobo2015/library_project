﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.BTN_Save = New System.Windows.Forms.Button()
        Me.TXT_fullname = New System.Windows.Forms.TextBox()
        Me.TXT_Age = New System.Windows.Forms.TextBox()
        Me.TXT_Address = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.BTN_OPTION = New System.Windows.Forms.Button()
        Me.GRID_Library = New System.Windows.Forms.DataGridView()
        Me.BTN_Update = New System.Windows.Forms.Button()
        Me.BTN_Delete = New System.Windows.Forms.Button()
        CType(Me.GRID_Library, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'BTN_Save
        '
        Me.BTN_Save.Location = New System.Drawing.Point(38, 231)
        Me.BTN_Save.Name = "BTN_Save"
        Me.BTN_Save.Size = New System.Drawing.Size(132, 43)
        Me.BTN_Save.TabIndex = 6
        Me.BTN_Save.Text = "Save"
        Me.BTN_Save.UseVisualStyleBackColor = True
        '
        'TXT_fullname
        '
        Me.TXT_fullname.Location = New System.Drawing.Point(38, 35)
        Me.TXT_fullname.Multiline = True
        Me.TXT_fullname.Name = "TXT_fullname"
        Me.TXT_fullname.Size = New System.Drawing.Size(237, 36)
        Me.TXT_fullname.TabIndex = 7
        '
        'TXT_Age
        '
        Me.TXT_Age.Location = New System.Drawing.Point(38, 97)
        Me.TXT_Age.Multiline = True
        Me.TXT_Age.Name = "TXT_Age"
        Me.TXT_Age.Size = New System.Drawing.Size(237, 36)
        Me.TXT_Age.TabIndex = 8
        '
        'TXT_Address
        '
        Me.TXT_Address.Location = New System.Drawing.Point(38, 164)
        Me.TXT_Address.Multiline = True
        Me.TXT_Address.Name = "TXT_Address"
        Me.TXT_Address.Size = New System.Drawing.Size(237, 36)
        Me.TXT_Address.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(38, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 20)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Fullname:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(38, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(42, 20)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Age:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(38, 141)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 20)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Address:"
        '
        'BTN_OPTION
        '
        Me.BTN_OPTION.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_OPTION.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTN_OPTION.Location = New System.Drawing.Point(260, 5)
        Me.BTN_OPTION.Name = "BTN_OPTION"
        Me.BTN_OPTION.Size = New System.Drawing.Size(43, 24)
        Me.BTN_OPTION.TabIndex = 13
        Me.BTN_OPTION.Text = "Option"
        Me.BTN_OPTION.UseVisualStyleBackColor = True
        '
        'GRID_Library
        '
        Me.GRID_Library.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GRID_Library.DefaultCellStyle = DataGridViewCellStyle1
        Me.GRID_Library.Location = New System.Drawing.Point(12, 316)
        Me.GRID_Library.MultiSelect = False
        Me.GRID_Library.Name = "GRID_Library"
        Me.GRID_Library.ReadOnly = True
        Me.GRID_Library.Size = New System.Drawing.Size(306, 190)
        Me.GRID_Library.TabIndex = 14
        '
        'BTN_Update
        '
        Me.BTN_Update.BackColor = System.Drawing.Color.Transparent
        Me.BTN_Update.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control
        Me.BTN_Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_Update.Location = New System.Drawing.Point(37, 287)
        Me.BTN_Update.Name = "BTN_Update"
        Me.BTN_Update.Size = New System.Drawing.Size(75, 23)
        Me.BTN_Update.TabIndex = 15
        Me.BTN_Update.Text = "Update"
        Me.BTN_Update.UseVisualStyleBackColor = False
        '
        'BTN_Delete
        '
        Me.BTN_Delete.BackColor = System.Drawing.Color.Transparent
        Me.BTN_Delete.FlatAppearance.BorderColor = System.Drawing.SystemColors.Control
        Me.BTN_Delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTN_Delete.Location = New System.Drawing.Point(118, 287)
        Me.BTN_Delete.Name = "BTN_Delete"
        Me.BTN_Delete.Size = New System.Drawing.Size(75, 23)
        Me.BTN_Delete.TabIndex = 16
        Me.BTN_Delete.Text = "Delete"
        Me.BTN_Delete.UseVisualStyleBackColor = False
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(330, 510)
        Me.Controls.Add(Me.BTN_Delete)
        Me.Controls.Add(Me.BTN_Update)
        Me.Controls.Add(Me.GRID_Library)
        Me.Controls.Add(Me.BTN_OPTION)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TXT_Address)
        Me.Controls.Add(Me.TXT_Age)
        Me.Controls.Add(Me.TXT_fullname)
        Me.Controls.Add(Me.BTN_Save)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Main"
        Me.Text = "Library Project"
        CType(Me.GRID_Library, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents BTN_Save As Button
    Friend WithEvents TXT_fullname As TextBox
    Friend WithEvents TXT_Age As TextBox
    Friend WithEvents TXT_Address As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents BTN_OPTION As Button
    Friend WithEvents GRID_Library As DataGridView
    Friend WithEvents BTN_Update As Button
    Friend WithEvents BTN_Delete As Button
End Class
