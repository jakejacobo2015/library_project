﻿Imports MySql.Data.MySqlClient

Public Class Options



    'Public Function StartMySqlConnection() As MySqlConnection
    '    Return New MySqlConnection("server=localhost;user id=root;password=;database=online_tech_db")
    'End Function

    Public Function StartMySqlConnection() As MySqlConnection
        Return New MySqlConnection("server='" & My.Settings.Server & "';user id='" & My.Settings.Username & "';password='" & My.Settings.Password & "';database='" & My.Settings.Database & "'")
    End Function
    Private Sub BTN_Save_Click(sender As Object, e As EventArgs) Handles BTN_Save.Click

        Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
        Try
            mySqlConnection.Open()
            If mySqlConnection.State = ConnectionState.Open Then
                    My.Settings.Server = TXT_Server.Text
                    My.Settings.Database = TXT_Databse.Text
                    My.Settings.Username = TXT_Username.Text
                    My.Settings.Password = TXT_Password.Text
                MsgBox("MySQL Database Sucessfully Connected.", vbInformation, "Connected")
            End If
        Catch ex As Exception
            MsgBox(ex.Message, vbCritical)
        End Try
        mySqlConnection.Close()

        Me.Hide()
        Main.Show()
    End Sub

    Private Sub Options_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If TXT_Server.Text = "" Then
            TXT_Server.Text = My.Settings.Server
        End If

        If TXT_Databse.Text = "" Then
            TXT_Databse.Text = My.Settings.Database
        End If

        If TXT_Username.Text = "" Then
            TXT_Username.Text = My.Settings.Username
        End If

        If TXT_Password.Text = "" Then
            TXT_Password.Text = My.Settings.Password
        End If
    End Sub
End Class