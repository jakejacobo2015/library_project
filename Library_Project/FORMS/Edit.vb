﻿Imports MySql.Data.MySqlClient

Public Class Edit

    Public StringResult As String
    Public mySqlCommand As New MySqlCommand
    Public mySqlAdapter As New MySqlDataAdapter
    Public mySqlDataTable As New DataTable


    Public Function StartMySqlConnection() As MySqlConnection
        Return New MySqlConnection("server='" & My.Settings.Server & "';user id='" & My.Settings.Username & "';password='" & My.Settings.Password & "';database='" & My.Settings.Database & "'")
    End Function

    Private Sub Update_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TXT_fullname.Text = Main.GRID_Library.Rows(Main.GRID_Library.CurrentRow.Index).Cells(0).Value.ToString
        TXT_Age.Text = Main.GRID_Library.Rows(Main.GRID_Library.CurrentRow.Index).Cells(1).Value.ToString
        TXT_Address.Text = Main.GRID_Library.Rows(Main.GRID_Library.CurrentRow.Index).Cells(2).Value.ToString
        TXT_UID.Text = Main.GRID_Library.Rows(Main.GRID_Library.CurrentRow.Index).Cells(3).Value.ToString
        TXT_UID.Visible = False
    End Sub




    Private Sub BTN_Update_Click(sender As Object, e As EventArgs) Handles BTN_Update.Click

        ' updatess("UPDATE users SET `name` = '" & TXTNAME.Text & "', `username` = '" & TXTUSERNAME.Text'& "', `Pass` = '" & TXTPASSWORD.Text & "',`type` = '" & CBOTYPE.Text & "' where id = '" & lblid.Text & "'")
        '        Catch ex As Exception
        Dim mySqlConnection As MySqlConnection = StartMySqlConnection()
        Try
            mySqlConnection.Open()
            With mySqlCommand
                .Connection = mySqlConnection
                .CommandText = "UPDATE online_tech_tbl SET `db_col_fullname`='" & TXT_fullname.Text & "', `db_col_age`='" & TXT_Age.Text & "',`db_col_address`='" & TXT_Address.Text & "' WHERE db_col_uid='" & TXT_UID.Text & "'"

                Dim mysql_result = mySqlCommand.ExecuteNonQuery
                If mysql_result = 0 Then
                    MsgBox("Data is NOT  Updated to MySQL Database", vbCritical)
                Else
                    Dim UpdatedmySqlDataTable As New DataTable
                    mySqlConnection.Close()
                    Main.PopulateTable(UpdatedmySqlDataTable, mySqlConnection)
                    MsgBox("Data has been Updated to MySQL Database", vbInformation)
                End If
            End With

            Me.Hide()
            Main.Show()

        Catch ex As Exception
            MsgBox(ex.Message, vbCritical)
        End Try


    End Sub

    Private Sub BTN_Cancel_Click(sender As Object, e As EventArgs) Handles BTN_Cancel.Click
        TXT_UID.Text = ""
        TXT_fullname.Text = ""
        TXT_Age.Text = ""
        TXT_Address.Text = ""
        Me.Hide()
        Main.Show()
    End Sub
End Class