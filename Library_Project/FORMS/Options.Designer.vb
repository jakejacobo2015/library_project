﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Options
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Options))
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TXT_Username = New System.Windows.Forms.TextBox()
        Me.TXT_Databse = New System.Windows.Forms.TextBox()
        Me.TXT_Server = New System.Windows.Forms.TextBox()
        Me.BTN_Save = New System.Windows.Forms.Button()
        Me.TXT_Password = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(23, 140)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(87, 20)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Username:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(23, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 20)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Database:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(23, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 20)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Server:"
        '
        'TXT_Username
        '
        Me.TXT_Username.Location = New System.Drawing.Point(23, 163)
        Me.TXT_Username.Multiline = True
        Me.TXT_Username.Name = "TXT_Username"
        Me.TXT_Username.Size = New System.Drawing.Size(237, 36)
        Me.TXT_Username.TabIndex = 16
        '
        'TXT_Databse
        '
        Me.TXT_Databse.Location = New System.Drawing.Point(23, 96)
        Me.TXT_Databse.Multiline = True
        Me.TXT_Databse.Name = "TXT_Databse"
        Me.TXT_Databse.Size = New System.Drawing.Size(237, 36)
        Me.TXT_Databse.TabIndex = 15
        '
        'TXT_Server
        '
        Me.TXT_Server.Location = New System.Drawing.Point(23, 34)
        Me.TXT_Server.Multiline = True
        Me.TXT_Server.Name = "TXT_Server"
        Me.TXT_Server.Size = New System.Drawing.Size(237, 36)
        Me.TXT_Server.TabIndex = 14
        '
        'BTN_Save
        '
        Me.BTN_Save.Location = New System.Drawing.Point(23, 326)
        Me.BTN_Save.Name = "BTN_Save"
        Me.BTN_Save.Size = New System.Drawing.Size(132, 43)
        Me.BTN_Save.TabIndex = 13
        Me.BTN_Save.Text = "Save"
        Me.BTN_Save.UseVisualStyleBackColor = True
        '
        'TXT_Password
        '
        Me.TXT_Password.Location = New System.Drawing.Point(23, 246)
        Me.TXT_Password.Multiline = True
        Me.TXT_Password.Name = "TXT_Password"
        Me.TXT_Password.Size = New System.Drawing.Size(237, 36)
        Me.TXT_Password.TabIndex = 20
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(19, 223)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 20)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Password"
        '
        'Options
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(345, 396)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TXT_Password)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TXT_Username)
        Me.Controls.Add(Me.TXT_Databse)
        Me.Controls.Add(Me.TXT_Server)
        Me.Controls.Add(Me.BTN_Save)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Options"
        Me.Text = "Options"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents TXT_Username As TextBox
    Friend WithEvents TXT_Databse As TextBox
    Friend WithEvents TXT_Server As TextBox
    Friend WithEvents BTN_Save As Button
    Friend WithEvents TXT_Password As TextBox
    Friend WithEvents Label4 As Label
End Class
